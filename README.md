**Description**

This is a work in progress portfolio website to showcase my ability to use React and .NET.  
It feature a currency converter function which communicates with a web API.  
It also features a ToDo list which writes to an in-memory database. 