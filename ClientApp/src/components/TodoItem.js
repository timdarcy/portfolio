import React, { Component } from 'react';
import { ListGroupItem, Button } from 'reactstrap';
export class TodoItem extends Component {
    constructor(props) {
        super(props)
        this.state = { id: props.id, name: props.name };
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        fetch("/api/todo/" + this.state.id, {
            method: "DELETE",
        }).then(this.props.retriveItems);

    }

    render() {
        return (
            <div>
                <ListGroupItem >{this.state.name}</ListGroupItem> <Button onClick={this.handleDelete}>Delete</Button>
            </div>
        );

    }
}