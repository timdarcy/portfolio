import React, { Component } from 'react';
import { Form, FormGroup, Input, Button, ListGroup, ListGroupItem } from 'reactstrap';
import { TodoItem } from './TodoItem';
export class TodoList extends Component {
    constructor(props) {
        super(props);
        this.state = { TodoList: [] }
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleInput = this.handleInput.bind(this);
        this.retrieveItems = this.retrieveItems.bind(this);
    }

    handleInput(event) {
        this.setState({ itemToAdd: event.target.value });
        console.log(event.target.value);
    }

    handleDelete(item) {
        //handles delete click
        console.log(item)
    }
    handleAdd() {
        console.log(this.state.itemToAdd);
        let newItem = this.state.itemToAdd;
        let dataToSend = { Name: newItem, isComplete: false }
        console.log(dataToSend);
        fetch("/api/todo", {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json",
            },
            redirect: "follow",
            referrer: "no-referrer",
            body: JSON.stringify(dataToSend)
        }).then(this.retrieveItems());

    }
    retrieveItems() {
        fetch("/api/todo").then((res) => {
            res.json().then((itemList) => {
                let itemListObject = {}
                //each list item needs the Id to be the key
                for (let count = 0; count < itemList.length; count++) {
                    itemListObject[itemList[count].id] = { name: itemList[count].name, IsComplete: itemList[count].IsComplete }
                }
                console.log(itemListObject)
                this.setState({ TodoList: itemListObject });
                console.log(this.state.TodoList)
            })

        });
    }

    componentDidMount() {
        this.retrieveItems();
    }
    render() {
        return (
            <div>
                <h2>Todo List</h2>
                <Form>
                    <FormGroup>
                        <Input placeholder="Enter do to item" onChange={this.handleInput}></Input>
                    </FormGroup>
                    <Button outline color="info" onClick={this.handleAdd}>Add item</Button>
                </Form>

                <ListGroup>
                    <div>
                        {
                            Object.keys(this.state.TodoList).map((key) =>

                                <TodoItem key={key} retriveItems={this.retrieveItems} name={this.state.TodoList[key].name} id={key}></TodoItem>


                            )}
                    </div>
                    <ListGroupItem>{Object.keys(this.state.TodoList).length}</ListGroupItem>
                </ListGroup>

            </div>
        );
    }
}
