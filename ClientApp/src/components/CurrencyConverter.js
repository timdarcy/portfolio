import React, { Component } from 'react';
import { Form, FormGroup, Label, Input, option } from 'reactstrap';
import './CurrencyConverter.css';

export class CurrencyConverter extends Component {
    constructor(props) {
        super(props);
        this.state = { currencies: ["AUD", "USD", "EUR", "GBP"], currencyFrom: "AUD", currencyTo: "AUD", amountFrom: 0, amountTo: 0 }
        this.handleSelect = this.handleSelect.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInput = this.handleInput.bind(this);
    }


    handleSubmit(event) {
        event.preventDefault();
        console.log(event);
        //https://api.exchangeratesapi.io/ provides free and open source exchange rates
        fetch("https://api.exchangeratesapi.io/latest?base=" + this.state.currencyFrom).then((res) => {
            res.json().then((result) => {
                let rate = result.rates[this.state.currencyTo]
                let convertedAmount = (rate * this.state.amountFrom).toFixed(2);
                //console.log(rate);
                //console.log(convertedAmount);
                this.setState({ amountTo: convertedAmount });
                //console.log(this.state.amountTo);
            });
        });


    }
    handleSelect(event) {
        console.log(event.target.name, event.target.value);
        this.setState({ [event.target.name]: event.target.value });

    }

    handleInput(event) {
        this.setState({ [event.target.name]: event.target.value })
    }
    render() {
        return (
            <Form className="mw-50" onSubmit={this.handleSubmit}>
                <FormGroup>
                    <Label>Select current currency</Label>
                    <Input type="select" name="currencyFrom" value={this.state.currencyFrom} onChange={this.handleSelect}>
                        {this.state.currencies.map((name, index) => {
                            return <option key={index} value={name} >{name}</option>
                        })}
                    </Input>
                    <Input type="number" step="0.01" name="amountFrom" onChange={this.handleInput}></Input>
                </FormGroup>

                <FormGroup>
                    <Label>Select target currency</Label>
                    <Input type="select" name="currencyTo" value={this.state.currencyTo} onChange={this.handleSelect}>
                        {this.state.currencies.map((name, index) => {
                            return <option key={index} value={name} >{name}</option>
                        })}
                    </Input>
                </FormGroup>


                <Input className="btn btn-info" type="submit" value="Convert" onChange={this.handleInput} />
                <Label>Converted Amount:</Label>
                <div type="number" name="amountFrom" onChange={this.handleInput}>${this.state.amountTo}</div>
            </Form >
        );
    }
}