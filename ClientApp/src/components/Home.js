import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render() {
    return (
      <div>
        <h1>Welcome</h1>
        <p>A website built with React and ASP.NET</p>
        <p>See the Currency Converter feature or perhaps add to my to do list</p>
      </div>
    );
  }
}
