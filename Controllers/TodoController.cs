using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Portfolio.Model;

namespace Portfolio.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private readonly TodoContext _context;

        public TodoController(TodoContext context)
        {
            _context = context;
            if (_context.TodoItems.Count() == 0)
            {
                // Create a new TodoItem if collection is empty,
                // which means you can't delete all TodoItems.
                _context.TodoItems.Add(new TodoItem { Name = "Item1", IsComplete = false });
                _context.SaveChanges();
            }

        }

        // GET: api/todo
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetTodoItem()
        {
            return await _context.TodoItems.ToListAsync();
        }
        // POST: api/todo
        [HttpPost]
        public async Task<ActionResult<TodoItem>> PostTodoItem(TodoItem newItem)
        {
            _context.TodoItems.Add(newItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTodoItem", new { id = newItem.Id }, newItem);
        }

        // PUT: api/todo/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTodoItem(long id, TodoItem updatedItem)
        {
            if (id != updatedItem.Id)
            {
                return BadRequest();
            }
            _context.Entry(updatedItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // DELETE: api/todo/1
        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteTodoItem(long id)
        {
            var itemToDelete = await _context.TodoItems.FindAsync(id);
            if (itemToDelete == null)
            {
                return NotFound();
            }

            _context.TodoItems.Remove(itemToDelete);
            await _context.SaveChangesAsync();

            return itemToDelete;
        }
    }


}